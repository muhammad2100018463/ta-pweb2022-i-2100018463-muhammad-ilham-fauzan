<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mengurutkan array</title>
    <style>
        .container{
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: row;
            margin: 10px;
        }
        .contents{
            border: 1px solid black;
            border-radius: 5px;
            padding: 8px;
            background-color: #646FD4;
            margin: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="contents">
            <?php
                $warna = array("merah","kuning","hijau","biru","hitam");
                $nilai = array("andi"=>90,"budi"=>55,"caca"=>37,"didi"=>60, "ega"=>80, "fafa"=>100);

                echo "Menampilkan array dengan foreach <br>";
                foreach ($warna as $key => $value) {
                    echo "Warna : $key = $value <br>";
                }

                echo "<br><hr>";
                echo "Menampilkan array nama nilai dengan while <br>";
                $i = 0;
                while ($i < count($nilai)) {
                    echo "Nama : ".key($nilai)." = ".current($nilai)."<br>";
                    next($nilai);
                    $i++;
                }
            ?>
        </div>
        
        <div class="contents">
            <?php
                echo "Mencetak struktur array <br>";
                echo "<pre>";
                print_r($warna);
                echo "<br><hr>";
                print_r($nilai);
                echo "</pre>";

                
            ?>
        </div>
        
        <div class="contents">
            <?php
                
                echo "Mengurutkan array dengan sort <br>";
                sort($warna);
                reset($warna);
                echo "<pre>";
                print_r($warna);
                echo "</pre>";

                echo "<br><hr>";
                echo "Mengurutkan array dengan rsort <br>";
                rsort($warna);
                reset($warna);
                echo "<pre>";
                print_r($warna);
                echo "</pre>";
            ?>
        </div>

        <div class="contents">
            <?php
                echo "Mengurutkan array dengan asort <br>";
                asort($nilai);
                reset($nilai);
                echo "<pre>";
                print_r($nilai);
                echo "</pre>";

                echo "<br><hr>";
                echo "Mengurutkan array dengan arsort <br>";
                arsort($nilai);
                reset($nilai);
                echo "<pre>";
                print_r($nilai);
                echo "</pre>";
            ?>
        </div>

        <div class="contents">
            <?php
                echo "Mengurutkan array dengan ksort <br>";
                ksort($nilai);
                reset($nilai);
                echo "<pre>";
                print_r($nilai);
                echo "</pre>";

                echo "<br><hr>";
                echo "Mengurutkan array dengan krsort <br>";
                krsort($nilai);
                reset($nilai);
                echo "<pre>";
                print_r($nilai);
                echo "</pre>";
            ?>
        </div>

        <div class="contents">
            <?php
                echo "Mengatur posisi pointer dalam array <br>";
                $kotaJabar = array("Jakarta","Bandung","Bekasi","Depok","Tangerang");
                echo "<pre>";
                print_r($kotaJabar);
                echo "</pre>";
                $mode = current($kotaJabar);
                echo $mode."<br>";
                next($kotaJabar);
                $mode = current($kotaJabar);
                echo $mode."<br>";
                next($kotaJabar);
                $mode = current($kotaJabar);
                echo $mode."<br>";
                next($kotaJabar);
                $mode = current($kotaJabar);
                echo $mode."<br>";
                next($kotaJabar);
                $mode = current($kotaJabar);
                echo $mode."<br>";

                echo "<br><hr>";
                echo "Mencari element array";
                $kotaJabar = array("Jakarta","Bandung","Bekasi","Depok","Tangerang");
                if(in_array("Depok", $kotaJabar)){
                    echo "Kota Depok ada di array";
                }else{
                    echo "Kota Depok tidak ada di array";
                }
            ?>
        </div>
    </div>
    
</body>
</html>
