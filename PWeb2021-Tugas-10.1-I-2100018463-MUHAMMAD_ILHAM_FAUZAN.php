<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Konversi nilai</title>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label for="nilai">Nilai</label>
        <input type="text" name="nilai" id="nilai">
        <input type="submit" value="Konversi">

<?php
    if (isset($_POST['nilai'])) {
        $nilai = $_POST['nilai'];
        if ($nilai >= 4.00) {
            echo "Nilai Anda A";
        } elseif ($nilai >= 3.67) {
            echo "Nilai Anda A-";
        } elseif ($nilai >= 3.33) {
            echo "Nilai Anda B+";
        } elseif ($nilai >= 3.00) {
            echo "Nilai Anda B";
        } elseif ($nilai >= 2.67) {
            echo "Nilai Anda B-";
        } elseif ($nilai >= 2.33) {
            echo "Nilai Anda C+";
        } elseif ($nilai >= 2.00) {
            echo "Nilai Anda C";
        } elseif ($nilai >= 1.67) {
            echo "Nilai Anda D+";
        } elseif ($nilai >= 1.33) {
            echo "Nilai Anda D";
        } elseif ($nilai >= 1.00) {
            echo "Nilai Anda E";
        } else {
            echo "Nilai Anda E";
        }
    }
?>
</body>
</html>