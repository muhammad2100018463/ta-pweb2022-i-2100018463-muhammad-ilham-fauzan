<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Piramid php</title>
</head>
<body>

<!-- buat form tinggi piramida -->
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
    <label for="tinggi">Tinggi piramida</label>
    <input type="text" name="tinggi" id="tinggi">
    <input type="submit" value="Hitung">
</form>

    <?php
        if (isset($_POST['tinggi'])) {
            $tinggi = $_POST['tinggi'];
            for($baris = 1;$baris<=$tinggi; $baris++){
                for($i = 1; $i<$tinggi-$baris;$i++){
                    echo "&nbsp;";
                }
                for($j = 1; $j<2*$baris; $j++){
                    echo "*";
                }
                echo "<br>";
            }
        }
    ?>
</body>
</html>