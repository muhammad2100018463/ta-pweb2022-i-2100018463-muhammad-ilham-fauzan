<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Penanganan form php</title>
    <style>
        *{
            box-sizing: border-box;
        }

        body{
            background-color: #f2d7d9;
        }
        .contents{
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            border: 1px solid black;
            border-radius: 5px;
            padding: 8px;
            background-color: #9CB4CC;
        }

        label{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="contents">
        <h2 align="center">Form Data Mahasiswa</h2>
        <form action="tugas-11-output.php" method="post">
            <div id="name">
                <label for="nama">Nama : </label><br>
                <input type="text" name="nama" placeholder="Inputkan Nama Anda">
            </div>
            <div id="nim">
                <label for="nim">Nim : </label><br>
                <input type="text" name="nim" placeholder="Inputkan Nim Anda">
            </div>
            <div id="alamat">
                <label for="alamat">Alamat : </label><br>
                <textarea name="alamat" id="alamat" cols="50" rows="10"></textarea>
            </div>
            <div id="prodi">
                <label for="prodi">jurusan : </label>
                <input type="radio" name="prodi" value="TI"><label for="prodi">Teknik informatika</label>
                <input type="radio" name="prodi" value="TE"><label for="prodi">Teknik Elektro</label>
                <input type="radio" name="prodi" value="TP"><label for="prodi">Teknik Pangan</label>
                <input type="radio" name="prodi" value="TK"><label for="Prodi">Teknik Kimia</label><br><br>
            </div>
            <div id="jenis-kelamin">
                <label for="jenis-kelamin">Jenis Kelamin :</label>
                <input type="checkbox" name="jenis-kelamin" value="pria"><label for="jenis-kelamin">Pria</label>
                <input type="checkbox" name="jenis-kelamin" value="wanita"><label for="jenis-kelamin">Wanita</label><br><br>
            </div>
            <div id="semester">
                <label for="semester">Semester :</label>
                <select name="semester" id="sems">
                    <option value="semester 1">Semester 1</option>
                    <option value="semester 2">Semester 2</option>
                </select>
            </div>
            <div id="submit-button">
                <input type="submit" value="Submit">
            </div>
        </form>
    </div>
</body>
</html>
