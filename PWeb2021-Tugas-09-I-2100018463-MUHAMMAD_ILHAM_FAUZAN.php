<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sintaks dasar php</title>
</head>
<body>
    
    <!-- listing program 9.1 -->
    <?php
        $gaji = 1000000;
        $pajak = 0.1;
        $thp = $gaji - ($gaji * $pajak);

        echo "Gaji sebelum pajak : Rp $gaji <br>";
        echo "Gaji yang dibawa pulang : Rp $thp";
    ?>

    <!-- listing program 9.2 -->
    <?php
        $a = 5;
        $b = 4;

        echo "$a == $b : ". ($a == $b);
        echo "<br>$a != $b : ". ($a != $b);
        echo "<br>$a > $b : ". ($a > $b);
        echo "<br>$a < $b : ". ($a < $b);
        echo "<br> ($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
        echo "<br> ($a == $b) || ($a > $b) : ". (($a != $b) || ($a > $b));
    ?>

</body>
</html>